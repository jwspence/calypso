#!/usr/bin/env python
"""Run tests on EcalGeoModel configuration

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior=1
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    from AthenaConfiguration.TestDefaults import defaultTestFiles

    ConfigFlags.Input.Files = defaultTestFiles.HITS
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"             # Always needed; must match FaserVersion
    ConfigFlags.Detector.EnableEcal = True
    ConfigFlags.GeoModel.Align.Dynamic    = False
    ConfigFlags.lock()

    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    from EcalGeoModel.EcalGeoModelConfig import EcalGeometryCfg
    acc = EcalGeometryCfg(ConfigFlags)
    f=open('EcalGeometryCfg.pkl','wb')
    acc.store(f)
    f.close()
