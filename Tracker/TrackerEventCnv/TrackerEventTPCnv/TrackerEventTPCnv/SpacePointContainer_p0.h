/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

#ifndef SPACEPOINTCONTAINER_P0_H
#define SPACEPOINTCONTAINER_P0_H

#include <vector>
#include "TrackerEventTPCnv/SpacePoint_p0.h"
#include "TrackerEventTPCnv/SpacePointCollection_p0.h"

class SpacePointContainer_p0 {
 public:
  SpacePointContainer_p0();
  friend class SpacePointContainerCnv_p0;
 private:
  std::vector<SpacePoint_p0> m_spacepoints;
  std::vector<SpacePointCollection_p0> m_spacepoint_collections;
};

inline SpacePointContainer_p0::SpacePointContainer_p0() {
  m_spacepoints.clear();
  m_spacepoint_collections.clear();
}

#endif  // SPACEPOINTCONTAINER_P0_H
