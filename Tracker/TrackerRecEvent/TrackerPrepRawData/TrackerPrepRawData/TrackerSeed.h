#ifndef TRACKERRAWDATA_TRACKERSEED_H
#define TRACKERRAWDATA_TRACKERSEED_H

#include "TrkSpacePoint/SpacePoint.h"

#include <vector>

using namespace std;

class MsgStream;

namespace Tracker {

  class TrackerSeed {

  public:

    enum StrategyId{NULLID=0, TRIPLET_SP_FIRSTSTATION=1};

    TrackerSeed();
    TrackerSeed(const StrategyId, const TrackerSeed &);
    virtual ~TrackerSeed();
    
    TrackerSeed(const StrategyId, vector<const Trk::SpacePoint*> seed);
    
    void       set_id(const StrategyId id) { m_strategyId = id; }
    StrategyId id() const                  { return m_strategyId; }

    void add(vector<const Trk::SpacePoint*> seed);
    
    int size() const;
    
    TrackerSeed &operator=(const TrackerSeed &);

    virtual MsgStream& dump(MsgStream& stream) const;
    virtual ostream&   dump(ostream&   stream) const;
    
  private:
    
    StrategyId m_strategyId;
    vector<const Trk::SpacePoint*> m_seed;
    
  };
  
  MsgStream& operator << (MsgStream& stream, const TrackerSeed& prd);
  ostream&   operator << (ostream&   stream, const TrackerSeed& prd);
  
}
#endif // TRACKERRAWDATA_TRACKERSEED_H
  
