/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TrackerGeoModelUtils_DetectorFactoryBase_H
#define TrackerGeoModelUtils_DetectorFactoryBase_H

#include "AthenaKernel/MsgStreamMember.h"
#include "GeoModelKernel/GeoVDetectorFactory.h" 
#include "TrackerGeoModelUtils/TrackerDDAthenaComps.h"

class StoreGateSvc;
class IGeoDbTagSvc;
class IRDBAccessSvc;

namespace TrackerDD {

class DetectorFactoryBase : public GeoVDetectorFactory  
{ 

public:
  DetectorFactoryBase(const TrackerDD::AthenaComps * athenaComps)
    : m_athenaComps(athenaComps)
  {}

  StoreGateSvc * detStore() const {return m_athenaComps->detStore();}

  IGeoDbTagSvc * geoDbTagSvc() const {return m_athenaComps->geoDbTagSvc();}

  IRDBAccessSvc * rdbAccessSvc() const {return m_athenaComps->rdbAccessSvc();}
  
  IGeometryDBSvc * geomDB() const {return m_athenaComps->geomDB();}

 //Declaring the Message method for further use
  MsgStream& msg (MSG::Level lvl) const { return m_athenaComps->msg(lvl); }

  //Declaring the Method providing Verbosity Level
  bool msgLvl (MSG::Level lvl) const { return m_athenaComps->msgLvl(lvl); }

  const TrackerDD::AthenaComps *  getAthenaComps() {return m_athenaComps;}

private:
  
  const TrackerDD::AthenaComps *  m_athenaComps;

};

} // end namespace

#endif // TrackerGeoModelUtils_DetectorFactoryBase_H

