/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "NeutrinoSimEventTPCnv/NeutrinoHits/NeutrinoHitCollectionCnv_p1.h"
#include "NeutrinoSimEventTPCnv/NeutrinoHits/NeutrinoHit_p1.h"
#include "NeutrinoHitCollectionCnv.h"


NeutrinoHitCollection_PERS* NeutrinoHitCollectionCnv::createPersistent(NeutrinoHitCollection* transCont) {
  MsgStream mlog(msgSvc(), "NeutrinoHitCollectionConverter" );
  NeutrinoHitCollectionCnv_PERS converter;
  NeutrinoHitCollection_PERS *persObj = converter.createPersistent( transCont, mlog );
  return persObj;
}


NeutrinoHitCollection* NeutrinoHitCollectionCnv::createTransient() {
    MsgStream mlog(msgSvc(), "NeutrinoHitCollectionConverter" );
    NeutrinoHitCollectionCnv_p1   converter_p1;

    static const pool::Guid   p1_guid("2CA7AF71-1494-4378-BED4-AFB5C54398AA");
    // static const pool::Guid   p1_guid("B2573A16-4B46-4E1E-98E3-F93421680779");

    NeutrinoHitCollection       *trans_cont(0);
    if( this->compareClassGuid(p1_guid)) {
      std::unique_ptr< NeutrinoHitCollection_p1 >   col_vect( this->poolReadObject< NeutrinoHitCollection_p1 >() );
      trans_cont = converter_p1.createTransient( col_vect.get(), mlog );
    }  else {
      throw std::runtime_error("Unsupported persistent version of Data container");
    }
    return trans_cont;
}
