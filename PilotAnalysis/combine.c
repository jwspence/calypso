#include "TFile.h"
#include "TNtuple.h"
#include "TLeaf.h"
#include "TDirectory.h"
#include<stdlib.h>
#include<iostream>
void combine(TString outputfile){
	TFile *g = new TFile(outputfile,"RECREATE");
	TNtuple *ntwrite = new TNtuple("ntuple","","id:plate:pid:x:y:z:tx:ty:w:chi2");
	TDirectory* dir = (TDirectory*)g;
	TString name;
	for(int fileidx=1;fileidx<7;fileidx++){
		name.Form("%d.root",fileidx);
		TFile f(name);
		TNtuple *nt = (TNtuple*)f.Get("ntuple");
		TLeaf *l = nt->FindLeaf("id");
		int nentries = nt->GetEntries();
		int id,plate,pid,w;
		double x,y,z,tx,ty,chi2;
		for(int i=0;i<nentries;i++){
			nt->GetEntry(i);
			id = l->GetValue(0);
			plate = l->GetValue(1);
			pid = l->GetValue(2);
			x = l->GetValue(3);
			y = l->GetValue(4);
			z = l->GetValue(5);
			tx = l->GetValue(6);
			ty = l->GetValue(7);
			w = l->GetValue(8);
			chi2 = l->GetValue(9);
			if(i%100000==0)std::cout << i << " " << id << " " << plate << " " << pid << " " << x << " " << y << " " << z << " " << tx << " " << ty << " " << w << " " << chi2 << std::endl;
			ntwrite->Fill(id,plate,pid,x,y,z,tx,ty,w,chi2);
		}
	}
	dir->cd();
	ntwrite->Write();
	g->Close();
}
