#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TNtuple.h"
#include <stdlib.h>
#include <iostream>
void digitizelinkedtracks(TString input,TString output){
	TFile f(input);
	TTree *t = (TTree*)f.Get("tracks");
	TBranch *b = t->FindBranch("s");
	TLeaf *idleaf = t->FindLeaf("s.eID");
	TLeaf *pidleaf = t->FindLeaf("s.ePID");
	TLeaf *izleaf = t->FindLeaf("s.eScanID.ePlate");	
	TLeaf *xleaf = t->FindLeaf("s.eX");
	TLeaf *yleaf = t->FindLeaf("s.eY");
	TLeaf *zleaf = t->FindLeaf("s.eZ");
	TLeaf *txleaf = t->FindLeaf("s.eTX");
	TLeaf *tyleaf = t->FindLeaf("s.eTY");
	TLeaf *wleaf = t->FindLeaf("s.eW");
	TLeaf *chi2leaf = t->FindLeaf("s.eChi2");
	std::cout << b->GetEntries() << std::endl;
	int nentries = b->GetEntries();
	TFile *g = new TFile(output,"RECREATE");
	TNtuple *nt = new TNtuple("ntuple","","id:pid:plate:x:y:z:tx:ty:w:chi2");
	int trackcount = 0;
	for(int i=0;i<nentries;i++){
		b->GetEntry(i);
		int llen = xleaf->GetLen();
		if(llen<4)continue;
		int iz0 = izleaf->GetValue(0);
		if(iz0>96)continue;
		double tx0 = txleaf->GetValue(0);
		double ty0 = tyleaf->GetValue(0);
		trackcount++;
		if(sqrt((tx0+0.008)*(tx0+0.008)+(ty0-0.028)*(ty0-0.028))>0.01)continue;
		for(int j=0;j<llen;j++){
			double x = xleaf->GetValue(j);
			double y = yleaf->GetValue(j);
			double z = zleaf->GetValue(j);
			double tx = txleaf->GetValue(j);
			double ty = tyleaf->GetValue(j);
			int id = idleaf->GetValue(j);
			int pid = pidleaf->GetValue(j);
			int plate = izleaf->GetValue(j);
			int w = wleaf->GetValue(j);
			double chi2 = chi2leaf->GetValue(j);
			if(i%10000==0)std::cout << i << " " << id << " " << pid << " " << plate << " " << x << " " << y << " " << z << " " << tx << " " << ty << " " << w << " " << chi2 << std::endl;
			nt->Fill(id,pid,plate,x,y,z,tx,ty,w,chi2);
		}
	}
	std::cout << trackcount << std::endl;
	nt->Write();
	g->Close();
}
