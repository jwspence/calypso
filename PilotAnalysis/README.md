This repository contains the source code used to perform pilot run analysis, electron clustering/energy regression.
The CP files are stored in /eos/project-f/fasernu-pilot-run/Data/TS2/TS2Pb/TS2Pb_full/areaXX
The linked_tracks files are stored in /eos/project-f/fasernu-pilot-run/Data/TS2/TS2Pb/ONLINE/b1000XX/dset_TS_p090-118

Compile compress.cpp against FEDRA libraries and run ./compress lnkY.def Y.root, 1<=Y<=6
root -l -q 'combine.c+("XXcp.root")', 0<=XX<=29
root -l -q 'digitizelinkedtracks.c+("linked_tracks.root","XXlink.root")', 0<=XX<=29
root -l -q 'subtract.c+("XXcp.root","XXlink.root","XXsubtracted.root")', 0<=XX<=29
root -l -q 'clstest.c+()'
