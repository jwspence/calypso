# $Id: CMakeLists.txt 744422 2016-05-03 11:34:39Z krasznaa $
################################################################################
# Package: xAODFaserBase
################################################################################

# Declare the package name:
atlas_subdir( xAODFaserBase )

# Extra dependencies based on what environment we are in:
if( NOT XAOD_STANDALONE )
   set( extra_deps Control/SGTools )
   set( extra_libs SGTools )
endif()

# External dependencies:
find_package( ROOT COMPONENTS Core Physics )

# Component(s) in the package:
atlas_add_library( xAODFaserBase
   xAODFaserBase/*.h Root/*.cxx
   PUBLIC_HEADERS xAODFaserBase
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers ${extra_libs}
   PRIVATE_LINK_LIBRARIES AthLinks )

# Test(s) in the package:
atlas_add_test( ut_xAODFaserObjectType_test
   SOURCES test/ut_xAODFaserObjectType_test.cxx
   LINK_LIBRARIES xAODFaserBase )
