/*
   Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/** @file PseduoSimToWaveformRecTool.h
 *  Header file for PseudoSimToWaveformRecTool.h
 *
 */
#ifndef WAVERECTOOLS_PSEUDOSIMTOWAVEFORMRECTOOL_H
#define WAVERECTOOLS_PSEUDOSIMTOWAVEFORMRECTOOL_H

//Athena
#include "AthenaBaseComps/AthAlgTool.h"
#include "WaveRecTools/IPseudoSimToWaveformRecTool.h"

//Gaudi
#include "GaudiKernel/ToolHandle.h"

//STL

class PseudoSimToWaveformRecTool: public extends<AthAlgTool, IPseudoSimToWaveformRecTool> {
 public:

  /// Normal constructor for an AlgTool; 'properties' are also declared here
 PseudoSimToWaveformRecTool(const std::string& type, 
			    const std::string& name, const IInterface* parent);

  /// Retrieve the necessary services in initialize
  StatusCode initialize();

 private:
  // None

};

#endif // WAVERECTOOLS_PSEUDOSIMTOWAVEFORMRECTOOL_H
